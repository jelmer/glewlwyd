glewlwyd (2.3.2-1) unstable; urgency=medium

  * New upstream release
  * [INTL:pt_BR] Brazilian Portuguese debconf templates translation
  * d/control: Remove dh-exec from builddep
  * d/gbp.conf: Add file and ignore unnecessary files in orig source
  * use upstream manpage
  * d/s/lintian-overrides: ignore lintian errrors
    source-contains-prebuilt-javascript-object and
    very-long-line-length-in-source-file

 -- Nicolas Mora <babelouest@debian.org>  Fri, 31 Jul 2020 07:59:25 -0400

glewlwyd (2.3.1-7) unstable; urgency=medium

  * Upload to unstable (Closes: #964136)

 -- Nicolas Mora <nicolas@babelouest.org>  Sun, 05 Jul 2020 18:00:58 -0400

glewlwyd (2.3.1-6) experimental; urgency=medium

  [Peter Michael Green]
  * Split architecture dependent and independent builds.

  [Nicolas Mora]
  * d/rules: remove webpack installed test in override_dh_auto_build
    since glewlwyd-common is now built in override_dh_auto_build-indep
  * d/rules: remove override_dh_auto_build as it is not overridden anymore

 -- Nicolas Mora <nicolas@babelouest.org>  Sun, 05 Jul 2020 13:17:36 -0400

glewlwyd (2.3.1-5) experimental; urgency=medium

  * d/control: use Build-Depends-Indep instead of [amd64] filters
  * d/rules: test if webpack is installed instead of testing host arch
  * d/rules: forbids to download dependencies if not found
  * d/control: suggests rnbyc for binary package glewlwyd

 -- Nicolas Mora <nicolas@babelouest.org>  Sun, 05 Jul 2020 07:47:07 -0400

glewlwyd (2.3.1-4) experimental; urgency=medium

  * Fully build glewlwyd-common only in amd64 arch so glewlwyd can be
    available in more architectures because as for now, arch:all
    packages are built in an amd64 arch
  * d/control: install frontend builddep only in amd64 arch
  * d/rules: build front-end only in amd64 arch

 -- Nicolas Mora <nicolas@babelouest.org>  Sat, 04 Jul 2020 10:49:17 -0400

glewlwyd (2.3.1-3) unstable; urgency=medium

  * d/control: remove unused packages

 -- Nicolas Mora <nicolas@babelouest.org>  Thu, 02 Jul 2020 07:25:46 -0400

glewlwyd (2.3.1-2) unstable; urgency=medium

  * Upload to unstable
  * Fix manpage version
  * [INTL:pt_BR] Brazilian Portuguese debconf templates translation
    (Closes: #938992)
  * Fix ftbfs with GCC-10 (Closes: #957274)
  * Move manpage to the proper section 8

 -- Nicolas Mora <nicolas@babelouest.org>  Fri, 26 Jun 2020 07:58:19 -0400

glewlwyd (2.3.1-1) experimental; urgency=medium

  * New upstream release
  * Use package bootstrap from Debian
  * Use package fonts-fork-awesome from Debian
  * d/postrm: remove config.json on purge
  * d/rules: disable verbose build

 -- Nicolas Mora <nicolas@babelouest.org>  Thu, 25 Jun 2020 19:36:55 -0400

glewlwyd (2.3.0-1) experimental; urgency=medium

  * New upstream release
  * d/control: Remove package font-font-awesome (Closes: #962875)
  * d/glewlwyd.postinst: limit postinst script to create database
    and initialize it, no key generation. Allow pgsql database creation
  * d/glewlwyd.init: Add SysV init file
  * d/copyright: Update entries
  * Bump debhelper from old 11 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * debian/control: bump standard to 4.5.0

 -- Nicolas Mora <nicolas@babelouest.org>  Fri, 05 Jun 2020 11:57:15 -0400

glewlwyd (1.4.9-3) unstable; urgency=medium

  * debian/control: bump standard to 4.4.0 (no changes)

 -- Nicolas Mora <nicolas@babelouest.org>  Sun, 25 Aug 2019 09:45:53 -0400

glewlwyd (1.4.9-2) unstable; urgency=medium

  * debian/glewlwyd.postinst: Change user glewlwyd home path to /etc/glewlwyd
  * debian/glewlwyd.postinst: Change permission for public key files to 644
  * debian/glewlwyd.postinst: Fix ecdsa files permission change
  * debian/control: upgrade Standards-Version to 4.3.0 (no change)
  * debian/control: add Multi-Arch: foreign for package glewlwyd-common
  * Fix ftbfs with GCC-9 (Closes: #925697)

 -- Nicolas Mora <nicolas@babelouest.org>  Fri, 04 Jan 2019 14:23:29 -0500

glewlwyd (1.4.9-1) unstable; urgency=medium

  * New upstream release (Closes: #916855)

 -- Nicolas Mora <nicolas@babelouest.org>  Sat, 15 Dec 2018 12:58:37 -0500

glewlwyd (1.4.6-3) unstable; urgency=medium

  * Fix Bug#911565: fails to install, purge, and install again
  * Fix Bug#911077: [INTL:fr] French debconf templates translation

 -- Nicolas Mora <nicolas@babelouest.org>  Mon, 22 Oct 2018 12:05:22 -0400

glewlwyd (1.4.6-2) unstable; urgency=medium

  * Fix Bug#905526: unowned files after purge
  * Fix lintian warning hardening-no-bindnow
  * Add metadata file

 -- Nicolas Mora <nicolas@babelouest.org>  Sun, 05 Aug 2018 15:18:47 -0400

glewlwyd (1.4.6-1) unstable; urgency=medium

  * New upstream release
  * debian/glewlwyd.postinst: don't check if/usr/share/dbconfig-common/dpkg/
                              files are present, assume they are
  * debian/glewlwyd.config: don't check if/usr/share/dbconfig-common/dpkg/
                            files are present, assume they are
  * Fix lintian warning maintainer-script-lacks-debhelper-token

 -- Nicolas Mora <nicolas@babelouest.org>  Thu, 19 Jul 2018 19:51:51 -0400

glewlwyd (1.4.4-3) unstable; urgency=medium

  * debian/postinst: improve auto config

 -- Nicolas Mora <nicolas@babelouest.org>  Sat, 09 Jun 2018 15:35:12 -0400

glewlwyd (1.4.4-2) unstable; urgency=medium

  * debian/postinst: force chmod 600 to jwt certificates generated
  * debian/config: remove note template due to lintian warning

 -- Nicolas Mora <nicolas@babelouest.org>  Fri, 01 Jun 2018 19:37:57 -0400

glewlwyd (1.4.4-1) unstable; urgency=medium

  * New upstream release
  * debian/postinst: Add dbconfig script

 -- Nicolas Mora <nicolas@babelouest.org>  Wed, 23 May 2018 23:31:51 -0400

glewlwyd (1.4.3-2) unstable; urgency=medium

  * debian/postinst: Improve install with templates

 -- Nicolas Mora <nicolas@babelouest.org>  Sat, 19 May 2018 14:43:33 -0400

glewlwyd (1.4.3-1) unstable; urgency=medium

  * New upstream release
  * debian/rules: Use cmake as buildsystem
  * debian/docs: Add documentation

 -- Nicolas Mora <nicolas@babelouest.org>  Mon, 30 Apr 2018 10:44:42 -0400

glewlwyd (1.4.0-1) unstable; urgency=medium

  * New upstream release
  * postinst: Add default config

 -- Nicolas Mora <nicolas@babelouest.org>  Mon, 02 Apr 2018 13:31:00 -0400

glewlwyd (1.3.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control: bump standard to 4.1.3 (no changes)
  * debian/control: use dh11

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 30 Jan 2018 19:05:19 +0100

glewlwyd (1.2.4-1) unstable; urgency=medium

  * New upstream release

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 21 Nov 2017 18:52:20 +0100

glewlwyd (1.2.2-1) unstable; urgency=medium

  * New upstream release
  * debian/control: bump standard to 4.1.1 (no changes)
  * debian/control: remove dependency on default-libmysqlclient-dev and
                    let libhoel-dev do the sql magic (Closes: #876714)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 07 Oct 2017 16:33:07 +0200

glewlwyd (1.2-1) unstable; urgency=medium

  * New upstream release
  * move config file to /etc/glewlwyd

 -- Thorsten Alteholz <debian@alteholz.de>  Fri, 15 Sep 2017 18:39:08 +0200

glewlwyd (1.1.2-1) unstable; urgency=medium

  * New upstream release
  * debian/control: bump standard to 4.1.0 (no changes)
  * debian/control: add VCS URLs
  * debian/control: set maintainer to team

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 05 Sep 2017 21:57:32 +0200

glewlwyd (1.1-2) unstable; urgency=medium

  * create fonts stuff within dh_install-indep (Closes: #871623)
  * put font package into fonts section (Closes: #871589)

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 16 Aug 2017 20:39:01 +0200

glewlwyd (1.1-1) unstable; urgency=medium

  * Initial release

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 07 Aug 2017 19:39:01 +0200
